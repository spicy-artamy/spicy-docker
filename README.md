
La version française de cette documentation est disponible sur notre [site web](https://www.artamy.com/spicy/documentation/fr/).


# SPICY Data Suite

The software suite at the service of your sensitive data, and to help you towards getting ready for GDPR audits.
Spicy Data Suite is made of two applications :

[Spicy Data Discovery](https://www.artamy.com/en/sensitive-data-discovery-and-classification/) : Discovery and Classification of Sensitive Data
[Spicy Process Register](https://www.artamy.com/en/register-of-data-processing-activities/): The Register and the Processing Activities Forms


## Technical Building Blocks

Spicy Data Suite is made of 3 technical Blocks :

- spicyManager : essential to the operation of Spicy Data Suite, it manages users, communication between applications, and configuration manager.
- spicyApp : This is the front-end building block of Spicy Data Suite
- spicyEngine : This is the heart of the Spicy Data Suite application, and performs all scan operations

In addition, Spicy Data Suite requires two databases, for spicyApp and spicyEngine.


## QuickStart

Launch all your infrastructure by running: `docker-compose up -d`.


### Configuration files

As a first step, you must download Spicy Data Suite configuration files, including the following:

Fichier  |  Description
---------|----------
README.md  | This very file !  Projet Description
docker-compose.yml  |  Docker configuration file, using the docker-compose format, which automatically downloads and executes Spicy Data Suite as docker containers.
spicy-config  | Folder containing Spicy Data Suite configuration files. The default configuration allows you to start a test environment. This configuration can be adapted for production.
spicy-docker-postgresql/  |  folder containing a docker script, allowing the creation of postgresql databases
pgdata1  |  will be used to persist postgresql data files.

##### Download using git

Install a recent version of git for your operating system.
Then, clone the project
```
git clone https://gitlab.com/spicy-artamy/spicy-docker.git
```

This command will create a `spicy-docker` folder, which will contain all the configuration files.

##### Download online

It is possible to download the configuration files from the gitlab website, the public repository of projects.

Visit the page: [https://gitlab.com/spicy-artamy/spicy-docker](https://gitlab.com/spicy-artamy/spicy-docker)


### Obtain access to dockers

Access to the dockers of Spicy Data Suite requires an up-to-date license.
So please:

1. Create a [dockerhub](https://hub.docker.com) account if you do not already have one.
2. Contact support by [mail](mailto: support@artamy.com) or from [support site](https://support.artamy.com), indicating your dockerhub ID
3. Your account will be allowed to access the dockers.



### Configure Spicy Data Suite

1. Please remove the spicy-postgres section of the file `docker-compose.yml`.
2. Create two databases (for spicyEngine and spicyApp) in [a supported database engine](https://www.artamy.com/spicy/documentation/en/installation/deploy/database/select-database/select-database-engine-for-spicy-data-suite/) .
3. Configure spicyEngine and spicyApp to connect to the new databases. The configuration files are `spicy-docker/spicy-config/spicyEngine-prod.yml ` and `spicy-docker/spicy-config/spicyApp-prod.yml` respectively.
4. Improve the applications' performance by proving more memory resources. to do so, uncomment the following lines in the `docker-compose.yml` file:

```
# - JAVA_OPTS=-Xmx2048m -Xms1024m
```

Please follow configuration steps in the [step by step installation chapter](https://www.artamy.com/spicy/documentation/en/installation/deploy/step-by-step/step-by-step-installation-of-spicy-data-suite/) to adapt the configuration to your needs and architecture.


### Install docker and docker-compose

The execution of the dockers provided requires a server containing docker and docker-compose binaries.
Please refer to the documentation for your operating system for the installation procedure.


### Start Spicy Data Suite

Go to the folder containing the configuration files

```
cd spicy-docker
```

Connect to docker hub with access provided by the support.
```
$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username (artamy): artamy
Password:
```

Download and run dockers

```
docker-compose up -d
```


### Connect to Spicy Data Suite

##### Graphical User Interface
The main connection url to Spicy Data Suite is: `https://<server>:8080`

The default login information:

- Login : admin
- password : admin

##### Interface Administration Technique

The Spicy Manager url, the Technical Administration interface : `https://<server>:8761  `

The default login information:

- Login : admin
- password : admin
